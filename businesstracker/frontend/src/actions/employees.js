import axios from "axios";

import { GET_EMPLOYEES, DELETE_EMPLOYEE, ADD_EMPLOYEE } from "./types";

// GET EMPLOYEES
export const getEmployees = () => dispatch => {
  axios
    .get("/api/api/employees/")
    .then(res => {
      dispatch({
        type: GET_EMPLOYEES,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

// DELETE EMPLOYEE
export const deleteEmployee = id => dispatch => {
  axios
    .delete(`/api/api/employees/${id}/`)
    .then(res => {
      dispatch({
        type: DELETE_EMPLOYEE,
        payload: id
      });
    })
    .catch(err => console.log(err));
};

// ADD EMPLOYEE
export const addEmployee = employee => dispatch => {
  axios
    .post("/api/api/employees/", employee)
    .then(res => {
      dispatch({
        type: ADD_EMPLOYEE,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};
