from rest_framework import serializers
from employees.models import Employee
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ()


class EmployeeSerializer(serializers.ModelSerializer):
    employee = UserSerializer(read_only=True)

    class Meta:
        model = Employee
        exclude = ()
