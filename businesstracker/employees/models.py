from django.db import models
from django.contrib.auth.models import User


class Employee(models.Model):
    employee = models.ForeignKey(
        User, related_name="employees", on_delete=models.CASCADE, null=True)
    mobile_phone = models.CharField(max_length=20, blank=True, null=True)
    home_phone = models.CharField(max_length=20, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    date_of_hire = models.DateField(blank=True, null=True)
    last_day_of_work = models.DateField(blank=True, null=True,
                                        help_text='*Only seen by Admin NEVER VISIBLE BY TEAM MEMBER')
    address = models.TextField(blank=True, null=True)
    emergency_contact_name = models.CharField(
        'Emergency Contact Name', blank=True, null=True, max_length=200)
    emergency_contact_mobile_phone = models.CharField(
        'Emergency Contact Mobile Phone', blank=True, null=True, max_length=200)
    emergency_contact_home_phone = models.CharField(
        'Emergency Contact Home Phone', blank=True, null=True, max_length=200)
    emergency_contact_address = models.TextField(
        'Emergency Contact Address', blank=True, null=True)
    biography = models.TextField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.employee.username
