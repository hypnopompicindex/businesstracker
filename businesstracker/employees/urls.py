from rest_framework import routers
from .api import EmployeeViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('api/employees', EmployeeViewSet, 'employees')
router.register('api/users', UserViewSet, 'users')

urlpatterns = router.urls
